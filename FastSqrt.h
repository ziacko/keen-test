﻿

public ref class FastMaths
{
public:
	static double inline __declspec (naked) FastSqrt(double value)
	{
		_asm fld qword ptr[esp + 4]
			_asm fsqrt
		_asm ret 8
	}

};