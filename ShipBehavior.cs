﻿using System.Collections.Generic;
using System.Threading;
using System.Linq;
using TestingTaskFramework;
using VRageMath;
using System;
using System.Runtime.CompilerServices;

namespace TestingTask
{
	// TODO: Modify 'OnUpdate' method, find asteroids in World (property Ship.World) and shoot them.
	class ShipBehavior : IShipBehavior
	{
		/// <summary>
		/// The ship which has this behavior.
		/// </summary>
		/// why are they named like this?

		public Ship Ship { get; set; }

		//the current targets
		public static List<WorldObject> targets = new List<WorldObject>();
		//asteroids that have already been fired on
		public static List<WorldObject> firedAt = new List<WorldObject>();

		public static double fireRadius = 0.0f;

		/// <summary>
		/// Called when ship is being updated, Ship property is never null when OnUpdate is called.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]//force it to be inline just in case the compiler doesnt do it automatically
		public void OnUpdate()
		{
			for (int iter = 0; iter < targets.Count; iter++)
			{
				if ((targets[iter].BoundingBox.Center - Ship.BoundingBox.Center).Length() > fireRadius)
				{
					targets.RemoveAt(iter);
				}
			}

			if (targets.Count > 1)
			{
				targets = targets.OrderByDescending(A => A.NeedsUpdate).ThenBy(A => Vector3.Distance(A.BoundingBox.Center, Ship.BoundingBox.Center)).ToList();

			}

			if (targets.Count > 0 && Ship.CanShoot)
			{
				Vector3 relativePosition = targets[0].BoundingBox.Center - Ship.BoundingBox.Center;
				Vector3 relativeVelocity = targets[0].LinearVelocity - Ship.LinearVelocity;

				double a = Vector3.Dot(relativeVelocity, relativeVelocity) - targets[0].LinearVelocity.LengthSquared() -
				(Ship.GunInfo.ProjectileSpeed * Ship.GunInfo.ProjectileSpeed) - (targets[0].BoundingRadius * targets[0].BoundingRadius);

				double b = 2.0f * Vector3.Dot(relativeVelocity, relativePosition);

				double c = Vector3.Dot(relativePosition, relativePosition);

				double p = -b / (2.0f * a);
				double q = Math.Sqrt((b * b) - 4.0f * a * c) / (2.0f * a);

				double t = 0.0f;
				double t1 = p - q;
				double t2 = p + q;

				if (t1 > t2 && t2 > 0.0f)
				{
					t = t2;
				}

				else
				{
					t = t1;
				}

				if (t > 0)
				{
					Vector3 aim = targets[0].BoundingBox.Center + targets[0].LinearVelocity * (float)t;
					Vector3 bulletPath = aim - (Ship.BoundingBox.Center + Ship.LinearVelocity * (float)t);

					double toCollision = bulletPath.Length() / Ship.GunInfo.ProjectileSpeed;

					//if the time it takes for the projectile to hit the target is greater than the lifetime of the projectile, skip
					if (toCollision > Ship.GunInfo.ProjectileLifetime.TotalSeconds)
					{
						targets.Remove(targets[0]);
					}

					else
					{
						Ship.Shoot(bulletPath);
						firedAt.Add(targets[0]);
						targets.Remove(targets[0]);
					}
				}
			}
		}
	}
}