﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using TestingTaskFramework;
using VRageMath;

namespace TestingTask
{
	// TODO: World is really slow now, optimize it.
	// TODO: Fix excessive allocations during run.
	// TODO: Write body of 'PreciseCollision' method.

	class World : IWorld
	{
		private static List<WorldObject> objects = new List<WorldObject>();
		public static Ship ship = null;

		/// <summary>
		/// Time of the world, increased with each update.
		/// </summary>
		public TimeSpan Time { get; private set; }

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static void ProcessList(object time)
		{
			for (int iter = 0; iter < objects.Count; iter++)
			{
				objects[iter].Update((TimeSpan)time);

				if (objects[iter].GetType() == typeof(Asteroid) && ship != null)
				{
					if (!ShipBehavior.targets.Contains(objects[iter]) &&
					!ShipBehavior.firedAt.Contains(objects[iter]))
					{
						double distance = Vector3.Distance(ship.BoundingBox.Center, objects[iter].BoundingBox.Center);
						if (distance < ShipBehavior.fireRadius)
						{
							ShipBehavior.targets.Add(objects[iter]);
						}
					}
				}
				
				if (iter < objects.Count && objects[iter] != null)
				{
					try
					{
						objects[iter].PostUpdate();
					}

					catch (NullReferenceException e)
					{

					}
				}
			}
		}

		/// <summary>
		/// Adds new object into world.
		/// World is responsible for calling OnAdded method on object when object is added.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Add(WorldObject obj)
		{
			if(obj.GetType() == typeof(Ship))
			{
				ship = obj as Ship;
				ShipBehavior.fireRadius = (ship.GunInfo.ProjectileSpeed * ship.GunInfo.ProjectileLifetime.TotalSeconds) + 5.0f;
			}

			objects.Add(obj);
			obj.OnAdded(this);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static void DeferredRemoveTarget(object paraneter)
		{
			//check if the asteroid is one that has been fired at.
			//if true then remove said asteroid
			Asteroid asteroid = paraneter as Asteroid;
			if (ShipBehavior.firedAt.Contains(asteroid))
			{ 
				ShipBehavior.firedAt.Remove(asteroid);
			}
		}

		/// <summary>
		/// Removes object from world.
		/// World is responsible for calling OnRemoved method on object when object is removed.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Remove(WorldObject obj)
		{
			if (obj.GetType() == typeof(Asteroid))
			{
				ThreadPool.QueueUserWorkItem(DeferredRemoveTarget, obj);
			}

			objects.Remove(obj);
			obj.OnRemoved();
		}

		/// <summary>
		/// Called when object is moved in the world.
		/// </summary>
		public void OnObjectMoved(WorldObject obj, Vector3 displacement)
		{
		}

		/// <summary>
		/// Clears whole world and resets the time.
		/// </summary>
		public void Clear()
		{
			Time = TimeSpan.Zero;
			ship = null;

			if (objects.Count > 0)
			{
				objects.Clear();
			}
		}

		/// <summary>
		/// Queries the world for objects in a box. Matching objects are added into result list.
		/// Query should return all overlapping objects.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Query(BoundingBox box, List<WorldObject> resultList)
		{
			for(int iter = 0; iter < objects.Count; iter++)
			{
				if (box.Contains(objects[iter].BoundingBox) != ContainmentType.Disjoint)
				{
					resultList.Add(objects[iter]);
				}
			}
		}

		/// <summary>
		/// Updates the world in following order:
		/// 1. Increase time.
		/// 2. Call Update on all objects with NeedsUpdate flag.
		/// 3. Call PostUpdate on all objects with NeedsUpdate flag.
		/// PostUpdate on first object must be called when all other objects are Updated.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Update(TimeSpan deltaTime)
		{
			Time += deltaTime;
			if (ship.TrajectoryProgress > 0.99f || ship.TrajectoryFinished == true)
			{
				for (int iter = 0; iter < objects.Count; iter++)
				{
					if (objects[iter].NeedsUpdate)
					{
						objects[iter].Update(deltaTime);
						objects[iter].PostUpdate();
					}
				}
			}

			else
			{
				ThreadPool.QueueUserWorkItem(ProcessList, deltaTime);
			}
		}

		/// <summary>
		/// Calculates precise collision of two moving objects.
		/// Returns exact delta time of touch (e.g. 1 is one second in future from now).
		/// When objects are already touching or overlapping, returns zero. When the objects won't ever touch, returns positive infinity.
		/// </summary>
		///

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public float PreciseCollision(WorldObject first, WorldObject second)
		{
			Vector3 betweenCenters = second.BoundingBox.Center - first.BoundingBox.Center;
			Vector3 relativeVelocity = (second.LinearVelocity - first.LinearVelocity);
			double sumRadii = second.BoundingRadius + first.BoundingRadius;
			double c = Vector3.Dot(betweenCenters, betweenCenters) - (sumRadii * sumRadii);

			//if c is less than 0 then they are already touching!
			if (c < 0.0f)
			{
				return 0.0f;
			}

			double a = Vector3.Dot(relativeVelocity, relativeVelocity);
			if(a < float.Epsilon)
			{
				//objects are not moving relative to each other
				return float.PositiveInfinity;
			}

			double b = Vector3.Dot(relativeVelocity, betweenCenters);
			if(b >= 0.0f)
			{
			//objects aren't moving towards each other
				return float.PositiveInfinity;
			}

			double d = (b * b) - a * c;
			if(d < 0.0f)
			{
				//spheres will not intersect? need to study why(graph paper time!!!)
				return float.PositiveInfinity;
			}

			return (float)((-b - Math.Sqrt(d)) / a);
		}
	}
}